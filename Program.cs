﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Homework_01
{
    class Program
    {
        static void Main(string[] args)
        {
            //Задание 1. Создаём репозиторий из 20 сотрудников
            Repository repository = new Repository(20);

            // Печать в консоль всех сотрудников
            repository.Print("Задание 1. БД до преобразования");

            // Увольнение всех работников с именем "Агата"
            repository.DeleteWorkerByName("Агата");

            // Печать в консоль сотрудников, которые не попали под увольнение
            repository.Print("Задание 1. БД волны увольнений");

            //Задание 2. Создаём репозиторий из 40 сотрудников
            Repository repository1 = new Repository(40);
            repository1.Print("Задание 2. БД до преобразования");
            // Увольнение работников
            repository1.DeleteWorkerByName("Аделина");
            repository1.DeleteWorkerByName("Агата");
            repository1.DeleteWorkerByName("Агнес");
            //Задание 2. БД после преобразоване
            repository1.Print("Задание 2. БД после волны увольнений");

            //Задание 3. Создаём репозиторий из 50 сотрудников
            Repository repository2 = new Repository(50);
            repository2.Print("Задание 3. БД до преобразования");
            //Задание №3. Метод, удаляющий всех сотрудников с определённой ЗП
            repository2.DeleteWorkerBySalary(30000);
            repository2.Print("Задание 3. БД после волны увольнений");
            // Печать в консоль сотрудников, которые не попали под увольнение


            #region Домашнее задание

            // Уровень сложности: просто
            // Задание 1. Переделать программу так, чтобы до первой волны увольнени в отделе было не более 20 сотрудников

            // Уровень сложности: средняя сложность
            // * Задание 2. Создать отдел из 40 сотрудников и реализовать несколько увольнений, по результатам
            //              которых в отделе болжно остаться не более 30 работников

            // Уровень сложности: сложно
            // ** Задание 3. Создать отдел из 50 сотрудников и реализовать увольнение работников
            //               чья зарплата превышает 30000руб



            #endregion
            Console.ReadKey();
        }
    }
}
